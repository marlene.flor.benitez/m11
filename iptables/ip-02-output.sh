# @MarleneFlor 2022-23
# iptables
#
# echo 1 > /proc/sys/net/ipv4/ip_forward

# regles flush
iptables -F
iptables -X
iptables -Z
iptables -t nat -F

# politica per defecte
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT
# obrir localhost
iptables -A INPUT  -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# obrir la nostra ip 
iptables -A INPUT  -s 10.200.245.215 -j ACCEPT
iptables -A OUTPUT -d 10.200.245.215 -j ACCEPT

# regles output
# ================================================
# podem accedir a qualsevol port 13 del mon
iptables -A OUTPUT -p tcp --dport 13 -j ACCEPT

# no accedir a cap 2013 de cap ordinador
iptables -A OUTPUT -p tcp --dport 2013 -j REJECT

# no accedir a cap 3013 de cap ordinador
iptables -A OUTPUT -p tcp --dport 3013 -j DROP

# port 4013 de ningú, aula si, g25 no
iptables -A OUTPUT -p tcp --dport 4013 -d 10.200.245.225 -j DROP
iptables -A OUTPUT -p tcp --dport 4013 -d 10.200.245.0/24 -j ACCEPT
iptables -A OUTPUT -p tcp --dport 4013 -j REJECT

# port 5013 a tot si, aula no, g15 si
iptables -A OUTPUT -p tcp --dport 5013 -d 10.200.245.225 -j ACCEPT
iptables -A OUTPUT -p tcp --dport 5013 -d 10.200.245.0/24 -j DROP
iptables -A OUTPUT -p tcp --dport 5013 -j ACCEPT

# al aula només es pot accedir al servei ssh (tots serveis xapats)
# iptables -A OUTPUT -p tcp --dport 22 -d 10.200.245.0/24 -j ACCEPT
# iptables -A OUTPUT -d 10.200.245.0/24 -j DROP
