# @MarleneFlor 2022-23
# iptables
#
# echo 1 > /proc/sys/net/ipv4/ip_forward

# regles flush
iptables -F
iptables -X
iptables -Z
iptables -t nat -F

# politica per defecte
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT
# obrir localhost
iptables -A INPUT  -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# obrir la nostra ip 
iptables -A INPUT  -s 10.200.245.215 -j ACCEPT
iptables -A OUTPUT -d 10.200.245.215 -j ACCEPT

# exemple regles input
# =================================================
# obrir a tothom al port 13
iptables -A INPUT -p tcp --dport 13 -j ACCEPT
# tancar a tothom al port 2013 (reject)
iptables -A INPUT -p tcp --dport 2013 -j REJECT 
# tancar a tothom el port 3013 (drop)
iptables -A INPUT -p tcp --dport 3013 -j DROP
# tancar al g25 el port 4013 (drop) però obert a tothom
iptables -A INPUT -p tcp --dport 4013 -s 10.200.245.225 -j DROP
iptables -A INPUT -p tcp --dport 4013 -j ACCEPT

# port tancat a tothom però obert a hisx2, però tancat a g25
iptables -A INPUT -p tcp --dport 5013 -s 10.200.245.225 -j REJECT
iptables -A INPUT -p tcp --dport 5013 -s 10.200.245.0/24 -j ACCEPT
iptables -A INPUT -p tcp --dport 5013 -j DROP

# port obert a tothom, tancat a hisx2, obert g25
iptables -A INPUT -p tcp --dport 6013 -s 10.200.245.225 -j ACCEPT
iptables -A INPUT -p tcp --dport 6013 -s 10.200.245.0/24 -j DROP
iptables -A INPUT -p tcp --dport 6013 -j ACCEPT

# tancar accés als ports del 3000 al 8000
#iptables -A INPUT -p tcp --dport 3000:8000 -j DROP

# barrera final (tancar ports 0:1024)
#iptables -A INPUT -p tcp --dport 1:1024 -j REJECT
