# @MarleneFlor 2022-23
# iptables
#
# echo 1 > /proc/sys/net/ipv4/ip_forward

# regles flush
iptables -F
iptables -X
iptables -Z
iptables -t nat -F

# politica per defecte
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT
# obrir localhost
iptables -A INPUT  -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# obrir la nostra ip 
iptables -A INPUT  -s 10.200.245.215 -j ACCEPT
iptables -A OUTPUT -d 10.200.245.215 -j ACCEPT

# Exemples de trafic related, established
# ===================================================
# Permetre navegar per la web
iptables -A OUTPUT -p tcp --dport 80 -j ACCEPT
iptables -A INPUT  -p tcp --sport 80 -m tcp -m state --state RELATED,ESTABLISHED -j ACCEPT
# som un servidor web
iptables -A INPUT  -p tcp --dport 80 -j ACCEPT
iptables -A OUTPUT -p tcp --sport 80 -m tcp -m state --state RELATED,ESTABLISHED -j ACCEPT
# som un servidor web, no accept g25
iptables -A INPUT  -p tcp --dport 80 -s 10.200.245.225 -j REJECT
iptables -A INPUT  -p tcp --dport 80 -j ACCEPT
iptables -A OUTPUT -p tcp --sport 80 -m tcp -m state --state RELATED,ESTABLISHED -j ACCEPT
