# @MarleneFlor 2022-23
# iptables
#
# echo 1 > /proc/sys/net/ipv4/ip_forward

# regles flush
iptables -F
iptables -X
iptables -Z
iptables -t nat -F

# politica per defecte
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT
# obrir localhost
iptables -A INPUT  -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# obrir la nostra ip 
iptables -A INPUT  -s 10.200.245.215 -j ACCEPT
iptables -A OUTPUT -d 10.200.245.215 -j ACCEPT

# fer NAT per les xarxes docker netA (21) i netB (19)
iptables -t nat -A POSTROUTING -s 172.19.0.0/16 -j MASQUERADE
iptables -t nat -A POSTROUTING -s 172.21.0.0/16 -j MASQUERADE
