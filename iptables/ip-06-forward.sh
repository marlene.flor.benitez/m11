# @MarleneFlor 2022-23
# iptables
#
# echo 1 > /proc/sys/net/ipv4/ip_forward

# regles flush
iptables -F
iptables -X
iptables -Z
iptables -t nat -F

# politica per defecte
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT
# obrir localhost
iptables -A INPUT  -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# obrir la nostra ip 
iptables -A INPUT  -s 10.200.245.215 -j ACCEPT
iptables -A OUTPUT -d 10.200.245.215 -j ACCEPT

# fer NAT per les xarxes docker netA (21) i netB (19)
iptables -t nat -A POSTROUTING -s 172.19.0.0/16 -j MASQUERADE
iptables -t nat -A POSTROUTING -s 172.21.0.0/16 -j MASQUERADE

# Regles forward
# ==========================================================
# NetA no pot accedir a NetB
#iptables -A FORWARD -s 172.21.0.0/16 -d 172.19.0.0/16 -j REJECT
# netA no pot accedir a hostB1
#iptables -A FORWARD -s 172.21.0.0/16 -d 172.19.0.2 -j REJECT
# netA no pot accedir a cap port 13
#iptables -A FORWARD -s 172.21.0.0/16 -p tcp --dport 13 -j REJECT
# Xarxa A no pot accedir al port 2013 de la xarxa B
#iptables -A FORWARD -s 172.21.0.0/16 -p tcp --dport 2013 -d 172.19.0.0/16 -j DROP
# Xarxa A pot navegar per internet però res més a l'exterior
# web 80, sortida ethernet publica
#iptables -A FORWARD -s 172.21.0.0/16 -o enp1s0 -p tcp --dport 80 -j ACCEPT
#iptables -A FORWARD -s 172.21.0.0/16 -o enp1s0 -j DROP
#iptables -A FORWARD -d 172.21.0.0/16 -i enp1s0 -p tcp --sport 80 -m state --state RELATED,ESTABLISHED -j ACCEPT
#iptables -A FORWARD -d 172.21.0.0/16 -i enp1s0 -j REJECT
# xarxa A pot accedir al port 2013 de totes les xarxes d'internet exterior, excepte l'aula 2hisx
iptables -A FORWARD -s 172.21.0.0/16 -p tcp --dport 2013 -o enp1s0 -d 10.200.245.0/24 -j REJECT
iptables -A FORWARD -s 172.21.0.0/16 -p tcp --dport 2013 -o enp1s0 -j ACCEPT
iptables -A FORWARD -s 10.200.245.0/24 -p tcp --sport 2013 -i enp1s0 -d 172.21.0.0/16 -j REJECT
