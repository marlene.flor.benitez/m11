# @MarleneFlor 2022-23
# iptables
#
# echo 1 > /proc/sys/net/ipv4/ip_forward

# regles flush
iptables -F
iptables -X
iptables -Z
iptables -t nat -F

# politica per defecte
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT
# obrir localhost
iptables -A INPUT  -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# obrir la nostra ip 
iptables -A INPUT  -s 10.200.245.215 -j ACCEPT
iptables -A OUTPUT -d 10.200.245.215 -j ACCEPT

# fer NAT a les xarxes internes
iptables -t nat -A POSTROUTING -s 172.19.0.0/16 -j MASQUERADE
iptables -t nat -A POSTROUTING -s 172.20.0.0/16 -j MASQUERADE
iptables -t nat -A POSTROUTING -s 172.21.0.0/16 -j MASQUERADE

# (1) La xarxa A només pot accedir als serveis ssh i daytime del router
iptables -A INPUT -s 172.21.0.0/16 -p tcp --dport 22 -j ACCEPT
iptables -A OUTPUT -d 172.21.0.0/16 -p tcp --dport 22 -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A INPUT -s 172.21.0.0/16 -p tcp --dport 13 -j ACCEPT
iptables -A OUTPUT -d 172.21.0.0/16 -p tcp --dport 13 -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A INPUT -s 172.21.0.0/16 -j REJECT

# La xarxa A només pot accedir del serveis de la DMZ al servei web

#iptables -A FORWARD -s 172.21.0.0/16 -d 172.20.0.2 -p tcp --dport 80 -j ACCEPT
#iptables -A FORWARD -d 172.21.0.0/16 -s 172.20.0.2 -p tcp --sport 80 -m state --state RELATED,ESTABLISHED -j ACCEPT
#iptables -A FORWARD -s 172.21.0.0/16 -d 172.20.0.0/16 -j REJECT

# (2) La xarxa A només pot accedir al exterior als serveis web, ssh i daytime

iptables -A FORWARD -s 172.21.0.0/16 -o enp1s0 -p tcp --dport 80 -j ACCEPT
iptables -A FORWARD -d 172.21.0.0/16 -i enp1s0 -p tcp --sport 80 -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -s 172.21.0.0/16 -o enp1s0 -p tcp --dport 22 -j ACCEPT
iptables -A FORWARD -d 172.21.0.0/16 -i enp1s0 -p tcp --sport 22 -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -s 172.21.0.0/16 -o enp1s0 -p tcp --dport 2013 -j ACCEPT
iptables -A FORWARD -d 172.21.0.0/16 -i enp1s0 -p tcp --sport 2013 -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -s 172.21.0.0/16 -o enp1s0 -j REJECT

# (3) La xarxa A només pot accedir del serveis de la DMZ al servei web
iptables -A FORWARD -s 172.21.0.0/16 -d 172.20.0.0/19 -p tcp --dport 80 -j ACCEPT
iptables -A FORWARD -d 172.21.0.0/16 -s 172.20.0.0/16 -p tcp --sport 80 -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -d 172.21.0.0/16 -s 172.20.0.0/19 -j DROP

# (4) redirigir els ports perquè des de l'exterior es tingui accés a: 

iptables -t nat -A PREROUTING -p tcp --dport 3001 -i enp1s0 -j DNAT --to 172.21.0.2:80
iptables -t nat -A PREROUTING -p tcp --dport 3002 -i enp1s0 -j DNAT --to 172.21.0.3:2013
iptables -t nat -A PREROUTING -p tcp --dport 3003 -i enp1s0 -j DNAT --to 172.19.0.2:2080
iptables -t nat -A PREROUTING -p tcp --dport 3004 -i enp1s0 -j DNAT --to 172.19.0.3:3013

# (5) s'habiliten els ports 4001 en endavant per accedir per ssh als ports ssh de: hostA1(4001), hostA2(4002), hostB1(4003), hostB2(4004).
iptables -t nat -A PREROUTING -p tcp --dport 4001 -j DNAT --to 172.21.0.2:22

