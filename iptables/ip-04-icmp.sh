# @MarleneFlor 2022-23
# iptables
#
# echo 1 > /proc/sys/net/ipv4/ip_forward

# regles flush
iptables -F
iptables -X
iptables -Z
iptables -t nat -F

# politica per defecte
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT
# obrir localhost
iptables -A INPUT  -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# obrir la nostra ip 
iptables -A INPUT  -s 10.200.245.215 -j ACCEPT
iptables -A OUTPUT -d 10.200.245.215 -j ACCEPT

# regles ICMP
# ==================================================
# no permetem fer pings al exterior desde la meva màquina
#iptables -A OUTPUT -p ICMP --icmp-type 8 -j DROP
# no permetem fer ping cap al g25 (nosaltres no podem fer ping al g25)
iptables -A OUTPUT -p icmp --icmp-type 8 -d 10.200.245.225 -j DROP
# la nostra màquina no respon al ping
#iptables -A OUTPUT -p icmp --icmp-type 0 -j DROP 
# la nostra màquina no permet rebre ping
iptables -A INPUT -p icmp --icmp-type 8 -j DROP
# ----------------------------------------------------
# regles explícites permeto fer ping a la meva màquina
iptables -A OUTPUT -p icmp --icmp-type 8 -j ACCEPT
iptables -A INPUT -p icmp --icmp-type 0 -j ACCEPT
# regles explícites per denegar la nostra màquina respondre pings
iptables -A INPUT -p icmp --icmp-type 8 -j DROP
iptables -A OUTPUT -p icmp --icmp-type  0 -j DROP

