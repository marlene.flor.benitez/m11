# TLS22:LDAPS

> **tls22:ldaps** --- imatge de final de ldap amb els mateixos usuaris que el ldap22:editat, però amb entitats del tipus posixGroup afegits en una ou=grups. Amb el slapd.conf amb els esquemes bàsics i certificats per *ldaps*

> Creació d'un container docker amb un servei ldap i la base de dades dc=edt,dc=org. Aquest servei permet l'accés segur via TLS/SSL amb ldaps i també starttls.

> A més implementa SubjectAlternativeName

## Crear imatge

```
docker build -t marleneflor/tls22:ldaps .
```

## Engegar container

```
docker run --rm --name ldap.edt.org -h ldap.edt.org -p 389:389 -p 636:636 -d marleneflor/tls22:ldaps
```

## Exemple de grup

```
dn: cn=alumnes,ou=grups,dc=edt,dc=org
objectClass: posixGroup
cn: alumnes
gidNumber: 600
description: grup de alumnes
memberUid: anna
memberUid: marta
```

## Configurar TLS/SSL en el *slapd.conf*

```
TLSCACertificateFile        /etc/ldap/certs/ca.crt
TLSCertificateFile          /etc/ldap/certs/servercert.ldap.crt
TLSCertificateKeyFile       /etc/ldap/certs/serverkey.ldap.pem
TLSVerifyClient       never
# TLSCipherSuite HIGH:MEDIUM:LOW:+SSLv2
```

## SubjectAlternativeName - Extencions - *ca.conf*

```
basicConstraints=CA:FALSE
extendedKeyUsage=serverAuth,emailProtection
subjectAltName=IP:172.17.0.2,IP:127.0.0.1,email:copy,DNS:ldapserver.exemple.org

```
